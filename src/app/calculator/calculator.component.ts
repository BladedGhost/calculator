import { Component, OnInit } from '@angular/core';
import { ButtonService } from '../services/button.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  calculation = '0';
  calculated = '';
  buttons: Array<any> = new Array<any>();
  constructor(private buttonService: ButtonService) { }

  ngOnInit() {
    this.getButtons();
  }

  add(value) {
    if(this.calculation.startsWith('0') && this.calculation.length > 0) {
      this.calculation = this.calculation.substr(1, this.calculation.length - 1);
    }
    if(value === 'c') {
      this.calculation = '';
      return;
    } else if(value === '=') {
      this.calculatedValue();
      return;
    } else if (value === 'Del') {
      this.calculation = this.calculation.substr(0, this.calculation.length - 1);
      if (this.calculation.length < 1) {
        this.calculation = '0';
      }
      return;
    }
    debugger
    this.calculation += value;
  }
  calculatedValue() {
    try {
      this.calculated = eval(this.calculation);
    }
    catch (ex) {
      this.calculated = 'Invalid Calculation';
    }
  }

  getButtons() {
    this.buttonService.getButtons().subscribe(x => {
      this.buttons = <Array<any>>x;
    });
  }
}
