import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ButtonService {

  constructor(private http: HttpClient) { }

  getButtons() {
    return this.http.get('../../assets/data/calculator-buttons.json').pipe(map(x => x));
  }
}
