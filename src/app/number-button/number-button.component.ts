import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-number-button',
  templateUrl: './number-button.component.html',
  styleUrls: ['./number-button.component.css']
})
export class NumberButtonComponent implements OnInit {
  @Input() character: string;
  @Output() charClick = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  addChar() {
    this.charClick.emit(this.character);
  }
}
