import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { CalculatorComponent } from './calculator/calculator.component';

export const routes: Routes = [
    {path: '**', component: CalculatorComponent}
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'});
